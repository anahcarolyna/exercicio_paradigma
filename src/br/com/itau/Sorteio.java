package br.com.itau;

import java.util.Random;

public class Sorteio {
    public static void main(String[] args) {
        //Exercicio 1
        Random random = new Random();
        int numero = 0;//random.nextInt(6) + 1;

        int contador = 0;
        while(contador == numero) {
            numero = random.nextInt(6) + 1;
            contador++;
        }
        System.out.println("Exercicio 1: O número sorteado é: " + numero);

        //Exercicio 2
        System.out.println("Inicio do exercicio 2");
        int[] numeroSorteio = new int [3];
        int somatorio = 0;

        for(int i = 0; i < numeroSorteio.length; i++){
            contador = 0;

            numeroSorteio[i] = random.nextInt(6)+1;

            while(contador == numeroSorteio[i]) {
                numeroSorteio[i] = random.nextInt(6) + 1;
                contador++;
            }

            System.out.println("Sorteio " + i  + " é igual a " +  numeroSorteio[i]);
            somatorio +=  numeroSorteio[i];
        }
        System.out.println("A somatória é igual a = " + somatorio);

        //Exercicio 3
        System.out.println("Inicio do exercicio 3");
        int contadorExerc3 = 0;

        while(contadorExerc3 < 3)
        {
            somatorio = 0;
            for(int i = 0; i < numeroSorteio.length; i++){
                contador = 0;

                numeroSorteio[i] = random.nextInt(6)+1;

                while(contador == numeroSorteio[i]) {
                    numeroSorteio[i] = random.nextInt(6) + 1;
                    contador++;
                }

                System.out.println("Sorteio " + i  + " é igual a " +  numeroSorteio[i]);
                somatorio +=  numeroSorteio[i];
            }
            System.out.println("A somatória é igual a = " + somatorio);
            contadorExerc3++;
        }


    }
}
